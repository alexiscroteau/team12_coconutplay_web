import axios from "axios";
import router from "@/router"

const HOST = "http://localhost";
const PORT = 3000;

const state = {
  token: "",
  isLoggedIn: false,
  profile:{
    username: "",
    email: "",
    name: "",
    firstname: "",
    birthdate: "",
    role: "Guest",
  },
};

const mutations = {
  setToken(state, { authToken }) {
    state.token = authToken;
    state.isLoggedIn = true;
  },
  setProfile(state, {userProfile}) {
    state.profile.username = userProfile.username;
    state.profile.email = userProfile.email;
    state.profile.name = userProfile.name;
    state.profile.firstname = userProfile.firstname;
    state.profile.birthdate = userProfile.birthdate;
    state.profile.role = userProfile.role;
  }

};

const actions = {
  async getToken({ commit }, { user }) {
    try {
      const response = await axios.post(`${HOST}:${PORT}/login`, user);
      const authToken = response.data;
      commit("setToken", { authToken });
      router.push({ name: "home" });
    } catch (e) {
      throw e;
    }
  },
  async register({ commit }, { user }) {
      try {
        let response = await axios.post(`${HOST}:${PORT}/register`, user);
        
        const loginUser = { username: user.username, password: user.password};
        response = await axios.post(`${HOST}:${PORT}/login`, loginUser);
        const authToken = response.data;
      commit("setToken", { authToken });
      } catch (e) {
          console.error(e);
      }
  },
  async getProfile({ commit }) {
    try{
      const config = {
        headers: {
          Authorization: "Bearer " + state.token
        }
      }
      const response = await axios.get(`${HOST}:${PORT}/profile/me`, config);
      const userProfile = response.data;
      commit("setProfile", {userProfile});
    } catch (e) {
      router.push({ name: "home" });
    }
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
