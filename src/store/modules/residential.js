import axios from "axios";
import router from "@/router"

const HOST = "http://localhost";
const PORT = 3000;

const state = {
  residentials: []
};

const mutations = {
  setResidentials(state, { residentials }) {
    state.residentials = residentials
  },
};

const actions = {
  async getResidentials({ commit }, {token}) {
    try {
        const config = {
            headers: {
              Authorization: "Bearer " + token
            }
        }
        const response = await axios.get(`${HOST}:${PORT}/residentiel`, config);
        const residentials = response.data;
        commit("setResidentials", { residentials });
    } catch(e) {
        router.push({ name: "home" });
    }
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
