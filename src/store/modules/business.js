import axios from "axios";
import router from "@/router"

const HOST = "http://localhost";
const PORT = 3000;

const state = {
  businesses: []
};

const mutations = {
  setBusinesses(state, { businesses }) {
    state.businesses = businesses
  },
};

const actions = {
  async getBusinesses({ commit }, {token}) {
    try {
        const config = {
            headers: {
              Authorization: "Bearer " + token
            }
        }
        const response = await axios.get(`${HOST}:${PORT}/affaires`, config);
        const businesses = response.data;
        commit("setBusinesses", { businesses });
    } catch(e) {
        router.push({ name: "home" });
    }
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
