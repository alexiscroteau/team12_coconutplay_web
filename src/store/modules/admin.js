import axios from "axios";
import router from "@/router"

const HOST = "http://localhost";
const PORT = 3000;

const state = {
  adminSettings: {
    maxAttempts: null,
    attemptDelay: null,
    blockAccess: null,
    passwordChange: null,
    minPasswordLength: null,
    requiresCaps: null,
    requiresNumber: null,
    requiresSpecial: null,
  }
};

const mutations = {
  setSettings(state, { settings }) {
    state.adminSettings.maxAttempts = settings.maxAttempts;
    state.adminSettings.attemptDelay = settings.maxAttempts;
    state.adminSettings.blockAccess = settings.blockAccess;
    state.adminSettings.passwordChange = settings.passwordChange;
    state.adminSettings.minPasswordLength = settings.minPasswordLength;
    state.adminSettings.requiresCaps = settings.requiresCaps;
    state.adminSettings.requiresNumber = settings.requiresNumber;
    state.adminSettings.requiresSpecial = settings.requiresSpecial;
  },
};

const actions = {
  async getAdminSettings({ commit }, {token}) {
    try {
        const config = {
            headers: {
              Authorization: "Bearer " + token
            }
        }
        const response = await axios.get(`${HOST}:${PORT}/admin`, config);
        const settings = response.data;
        commit("setSettings", { settings });
    } catch(e) {
        router.push({ name: "home" });
    }
  },
  async putAdminSettings({commit}, {token, adminSettings}) {
    try {
        console.log(token);
        console.log(adminSettings);
        const config = {
            headers: {
                Authorization: "Bearer " + token,
            },
        }
        const response = await axios.put(`${HOST}:${PORT}/admin`, {adminSettings}, config);
        const settings = response.data;
        commit("setSettings", { settings });
    } catch(e) {
        router.push({ name: "home" });
    }
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
