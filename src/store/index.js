import Vue from "vue";
import Vuex from "vuex";

//import item from "@/store/modules/monModule";
import gameTest from "@/store/modules/gameTest";
import category from "@/store/modules/category";
import comment from "@/store/modules/comment";
import user from "@/store/modules/user";
import admin from "@/store/modules/admin";
import residential from "@/store/modules/residential"
import business from "@/store/modules/business";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    gameTest,
    category,
    comment,
    user,
    admin,
    residential,
    business,
  },
});
